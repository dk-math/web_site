/** @type {import('next').NextConfig} */
const execSync = require("child_process").execSync;
const lastCommitCommand = "git rev-parse HEAD";
const nextConfig = {
  reactStrictMode: true,
  async generateBuildId() {
    return execSync(lastCommitCommand).toString().trim();
  },
  images: {
    domains: ["image.tmdb.org", "oaidalleapiprodscus.blob.core.windows.net"],
  },
};

module.exports = nextConfig;
