# 映画 GO

- 「日本一シンプルな映画検索サイト」
- サイト URL：https://eigago.com/

# 概要

- 映画のタイトル、監督名などのキーワードで映画情報を検索可能
- レスポンシブ対応

# 機能説明

- 映画検索
- 映画館地図検索
- お気に入り登録
- 映画生成 AI

### 1. 映画検索

![01映画検索_20240407](/uploads/07a4bbffe6ab0d745dce4284c0434918/01映画検索_20240407.mp4)

### 2. 映画館地図検索

![02映画館地図検索_20240407](/uploads/5f9f94fee5992a5a41cfae708931f5f3/02映画館地図検索_20240407.mp4)

### 3. お気に入り登録

![03お気に入り_20240407](/uploads/ac0c9c1efd14a7c505f1c07474befdb7/03お気に入り_20240407.mp4)

### 4. 映画生成 AI

![04映画生成AI_20240407](/uploads/bbecf129976f4ca41a2e2014462f1682/04映画生成AI_20240407.mp4)

### 5. スマホ画面

![05スマホ画面_20240407](/uploads/2d2513aec849e7113ebe99550e1789bf/05スマホ画面_20240407.mp4)

# 使用環境

- MacOS Ventura 13.3.1(a)
- TypeScript 5.0.4
- React 18.2.0
- Next.js 13.3.0
- NGINX 1.22.1
- AWS

# 使用 API

- TMDB API
  - 用途：映画検索
  - https://www.themoviedb.org/documentation/api/terms-of-use
- Serp API
  - 用途：映画館 Google 検索
  - https://serpapi.com/legal
- Google Maps Platform API
  - 用途：映画館地図検索
  - https://cloud.google.com/maps-platform/terms
- OpenAI API
  - 用途：映画生成 AI
  - https://openai.com/policies/usage-policies
- DeepL API
  - 用途：映画生成 AI へのリクエストの英語変換
  - https://developers.deepl.com/docs

# インフラ構成図

![スクリーンショット_2024-08-26_23.19.40](/uploads/37570a1b62273cad889a707697f098ee/スクリーンショット_2024-08-26_23.19.40.png)

# お問い合わせ先

- kdds.dev@gmail.com

# 文責

- daisuke kondo
