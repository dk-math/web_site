import Iframe from "react-iframe";
import styles from "../styles/HomePageModal.module.css";

type Props = {
  movieDetail: any;
  onClose: () => void;
};

export const HomePageModal: React.FC<Props> = ({ movieDetail, onClose }) => {
  return (
    <>
      <div className={styles["home-page-modal"]}>
        <div className={styles["close-button"]} onClick={onClose}>
          <span className={styles["batsu"]}></span>
        </div>
        <Iframe
          className={styles["home-page-iframe"]}
          url={movieDetail.movie.homepage}
          width={"100%"}
          height={"100%"}
        />
      </div>
    </>
  );
};

export default HomePageModal;
