import React, { useEffect, useState } from "react";

type Props = {
  position: any;
  nextMarkerPositions: any[];
  map: google.maps.Map | undefined;
};

const Marker: React.FC<Props> = ({ position, nextMarkerPositions, map }) => {
  const [marker, setMarker] = useState<google.maps.Marker | null>(null);

  useEffect(() => {
    // 次に描画するマーカーに使用するかどうかを判定するフラグ
    const nextMarkerFlag = nextMarkerPositions.some(
      (obj: any) => obj.lat === position.lat && obj.lng === position.lng,
    );
    const label = String(
      nextMarkerPositions.findIndex((obj) => obj.lat === position.lat && obj.lng === position.lng) +
        1,
    );

    // 描画するマーカーのラベルを更新する
    if (nextMarkerFlag) {
      marker?.setLabel(label);
    }

    // 描画するマーカーに使用しないならば非表示
    if (map && marker && !nextMarkerFlag) {
      marker?.setMap(null);
      setMarker(null);
    }

    if (map && !marker) {
      // 描画するマーカーに使用するならば表示
      if (nextMarkerFlag) {
        const newMarker = new google.maps.Marker({
          position,
          label: label,
          map,
        });
        setMarker(newMarker);
      }
    }
  }, [marker, map, nextMarkerPositions, position]);

  return null;
};

export default Marker;
