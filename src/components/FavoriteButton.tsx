import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import styles from "../styles/FavoriteButton.module.css";

interface FavoriteButtonProps {
  initialActive?: boolean;
  movie: any;
}

const FavoriteButton: React.FC<FavoriteButtonProps> = ({ initialActive = false, movie }) => {
  const [isActive, setIsActive] = useState<boolean>(initialActive);

  useEffect(() => {
    // コンポーネントがマウントされたときにローカルストレージからお気に入り映画を取得し、isActiveを更新する
    const favorites = JSON.parse(localStorage.getItem("favorites") || "[]");
    setIsActive(favorites.some((fav: any) => fav.id === movie.id)); // 映画IDで存在チェック
  }, [movie.id]);

  const handleClick = () => {
    const favorites = JSON.parse(localStorage.getItem("favorites") || "[]");
    const index = favorites.findIndex((fav: any) => fav.id === movie.id); // 映画IDで存在チェック
    if (index === -1) {
      favorites.push(movie);
    } else {
      favorites.splice(index, 1);
    }
    localStorage.setItem("favorites", JSON.stringify(favorites));
    setIsActive(!isActive);
  };

  return (
    <button className={styles["favorite-button"]} onClick={handleClick}>
      <FontAwesomeIcon
        icon={faStar as IconProp}
        style={{ color: isActive ? "rgba(218, 165, 32, 0.9)" : "gray" }}
      />
    </button>
  );
};

export default FavoriteButton;
