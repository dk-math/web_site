import { Burger, Drawer } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import Link from "next/link";
import { signIn, signOut } from "next-auth/react";
import styles from "../styles/Header.module.css";

type Props = {
  session: any;
};

export const Header: React.FC<Props> = ({ session }) => {
  const [isHamburgerOpened, { close: hamburgerClose, toggle: hamburgerToggle }] =
    useDisclosure(false);
  return (
    <>
      <div className={styles["header"]}>
        <Burger opened={isHamburgerOpened} onClick={hamburgerToggle} color="#fffffe" />
        <Drawer
          opened={isHamburgerOpened}
          onClose={hamburgerClose}
          zIndex={100}
          withCloseButton={true}
          classNames={{
            body: "p-0",
            inner: "w-[380px]",
          }}
        >
          <div className="px-4 pt-[78px] font-bold">
            <div className={styles["header-top"]}>
              <Link href="/" onClick={hamburgerClose}>
                <p>TOP</p>
              </Link>
            </div>
            <div className={styles["header-favorites"]}>
              <Link href="/favorites" onClick={hamburgerClose}>
                <p className={styles["header-favorites-title"]}>お気に入り</p>
              </Link>
            </div>
            <div className={styles["header-create"]}>
              <Link href="/create" onClick={hamburgerClose}>
                <p className={styles["header-create-title"]}>映画生成AI</p>
              </Link>
            </div>
            <div className={styles["header-sign"]}>
              {session ? (
                <div className={styles["sign-out"]}>
                  <a
                    onClick={() => {
                      signOut();
                    }}
                  >
                    Sign Out
                  </a>
                </div>
              ) : (
                <div className={styles["sign-in"]}>
                  <a onClick={() => signIn()}>Sign In</a>
                </div>
              )}
            </div>
          </div>
        </Drawer>
        <div className={styles["header-logo"]}>
          <Link href="/">
            <p>
              映画<span>GO</span>
            </p>
          </Link>
        </div>
      </div>
    </>
  );
};

export default Header;
