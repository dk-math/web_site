import Image from "next/image";
import Link from "next/link";
import tmdbLogo from "../../public/tmdb-logo.svg";
import xIcon from "../../public/x-twitter.svg";
import styles from "../styles/Footer.module.css";

export const Footer: React.FC = () => {
  return (
    <>
      <div className={styles["footer"]}>
        <div className={styles["footer-upper"]}>
          <div className={styles["tmdb-logo"]}>
            <p>
              <Image
                className={styles["tmdb-logo-img"]}
                src={tmdbLogo}
                width={40}
                height={40}
                alt={"Tmdb Logo"}
              />
            </p>
          </div>
          <div className={styles["footer-logo"]}>
            <Link href="/">
              <p>
                映画<span>GO</span>
              </p>
            </Link>
          </div>
          {/* <div className={styles["x-icon"]}>
            <p>
              <a>
                <span className={styles["x-svg"]}>
                  <Image
                    className={styles["x-icon-img"]}
                    src={xIcon}
                    width={22}
                    height={22}
                    alt={"X Logo"}
                  />
                </span>
              </a>
            </p>
          </div> */}
        </div>
        <div className={styles["footer-lower"]}>
          <div className={styles["tmdb-logo-text"]}>
            <p>This product uses the TMDB API but is not endorsed or certified by TMDB.</p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
