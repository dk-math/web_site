import React, { useEffect, useRef, useState } from "react";
import styles from "../styles/theaters.module.css";

type Props = {
  selectedArea: any;
  currentLocation: any;
  children: (map: google.maps.Map | undefined) => React.ReactNode;
};

const Map: React.FC<Props> = ({ selectedArea, currentLocation, children }) => {
  const ref = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<google.maps.Map>();
  const [area, setArea] = useState(selectedArea);

  // 最初にMapを表示する時の設定
  const DEFAULT = {
    CENTER: {
      lat: parseFloat(selectedArea.lat ?? currentLocation.lat),
      lng: parseFloat(selectedArea.lng ?? currentLocation.lng),
    } as google.maps.LatLngLiteral,
    ZOOM: 11,
  } as const;

  useEffect(() => {
    if (ref.current && (!map || JSON.stringify(area) !== JSON.stringify(selectedArea))) {
      const option = {
        center: DEFAULT.CENTER,
        zoom: DEFAULT.ZOOM,
      };
      setMap(new window.google.maps.Map(ref.current, option));
      setArea(Object.keys(selectedArea).length > 0 ? selectedArea : {});
    }
  }, [area, selectedArea, currentLocation, DEFAULT.CENTER, DEFAULT.ZOOM, map]);

  return (
    <>
      <div className={styles["theater-map"]} ref={ref} />
      {children(map)}
    </>
  );
};

export default Map;
