import Image from "next/image";
import noimage from "../../public/noimage.jpg";
import styles from "../styles/Background.module.css";

type Props = {
  movieList: any[];
};

export const Background: React.FC<Props> = (data: any) => {
  return (
    <>
      <div className={styles["background"]}>
        {data.movieList
          .filter((movie: any) => {
            return movie.poster_path !== null;
          })
          .map((movie: any, i: number) => {
            return (
              <Image
                key={i}
                className={styles["background-img"]}
                src={
                  movie.poster_path
                    ? `https://image.tmdb.org/t/p/w500${movie.poster_path}`
                    : noimage
                }
                alt={"Movie Poster"}
                width={500}
                height={500}
              />
            );
          })}
      </div>
    </>
  );
};

export default Background;
