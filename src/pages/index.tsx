import { GetServerSideProps } from "next";
import Head from "next/head";
import { useSession } from "next-auth/react";
import React from "react";
import { Background } from "../components/Background";
import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import styles from "../styles/index.module.css";

type Props = {
  movieList: any[];
};

const Top: React.FC<Props> = ({ movieList }) => {
  const { data: session } = useSession();
  return (
    <>
      <Head>
        <title>映画GO</title>
      </Head>
      <div className={styles["body"]}>
        <div className={styles["content"]}>
          <Header session={session} />
          <Background movieList={movieList} />
          <div className={styles["main"]}>
            <div className={styles["search-movie"]}>
              <h1>
                <span className={styles["indention"]}>日本一シンプルな</span>
                <span className={styles["indention"]}>映画検索サイト</span>
              </h1>
              <p>
                <span className={styles["indention"]}>映画の概要・監督・出演者・上映場所が</span>
                <span className={styles["indention"]}>すぐわかる!</span>
              </p>
              <form method="get" action="/movies" className={styles["search-container"]}>
                <input name="keyword" type="text" placeholder="作品名、監督名 を入力" required />
                <input type="submit" value="検索" className="fa-solid fa-magnifying-glass" />
              </form>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const resData = await fetch("http://localhost:3000/api");
  const movieList = await resData.json();
  return { props: { movieList } };
};

export default Top;
