import { Wrapper } from "@googlemaps/react-wrapper";
import { GetServerSideProps } from "next";
import Head from "next/head";
import Link from "next/link";
import Router from "next/router";
import { useSession } from "next-auth/react";
import React, { useState, useEffect } from "react";
import Select from "react-select";
import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import Map from "../components/Map";
import Marker from "../components/Marker";
import area from "../data/area";
import styles from "../styles/theaters.module.css";

type Props = {
  initialTheaterList: {
    day: string;
    date: string;
    theaters: {
      name: string;
      link: string;
      distance: string;
      address: string;
      showing: {
        time: string[];
        type: string;
      }[];
    }[];
  }[];
  initialTheaterLocationList: any[];
  movieTitle: string;
  googleMapApiKey: string;
};

type SelectedArea = {
  value?: string;
  label?: string;
  lat?: string;
  lng?: string;
};

const Theaters: React.FC<Props> = ({
  initialTheaterList,
  initialTheaterLocationList,
  movieTitle,
  googleMapApiKey,
}) => {
  const { data: session } = useSession();
  const [isLoading, setIsisLoading] = useState(true);
  const [currentLocation, setCurrentLocation] = useState({});
  const [theaterList, setTheaterList] = useState(initialTheaterList);
  const [theaterLocationList, setTheaterLocationList] = useState(initialTheaterLocationList);
  const [theaterDate, setTheaterDate] = useState(theaterList[0]?.date ?? "");
  const [activeDateIndex, setActiveDateIndex] = useState(0);
  const [nextMarkerPositions, setNextMarkerPositions] = useState(
    theaterLocationList[0]?.locationList ?? [],
  );
  const [preMarkerPositions, setPreMarkerPositions] = useState(nextMarkerPositions);
  const [selectedArea, setSelectedArea] = useState<SelectedArea>({
    value: "tokyo",
    label: "東京都",
    lat: "35.689185",
    lng: "139.691648",
  });
  const handleTheaterDate = (date: string) => {
    setTheaterDate(date);
  };
  const handleActive = (index: number) => {
    setActiveDateIndex(index);
  };
  const handlePositions = (positions: any) => {
    setPreMarkerPositions(nextMarkerPositions);
    setNextMarkerPositions(positions);
  };
  const searchWithArea = async (area: any) => {
    setSelectedArea(area);
    setActiveDateIndex(0);
    setIsisLoading(true);
    Router.push(`/theaters?title=${movieTitle}&lat=${area.lat}&lng=${area.lng}&city=${area.value}`);
    const theaterInfo = await fetch(
      `./api/theaters?title=${movieTitle}&lat=${area.lat}&lng=${area.lng}&country=JP&city=${area.value}`,
    ).then((res) => res.json());
    setTheaterList(theaterInfo.theaterList);
    setTheaterLocationList(theaterInfo.theaterLocationList);
    setTheaterDate(theaterInfo.theaterList[0]?.date ?? "");
    setNextMarkerPositions(theaterInfo.theaterLocationList[0]?.locationList ?? []);
    setPreMarkerPositions(theaterInfo.theaterLocationList[0]?.locationList ?? []);
    setIsisLoading(false);
  };

  const searchWithCurrentLocation = async (location: any) => {
    setIsisLoading(true);
    setSelectedArea({});
    setActiveDateIndex(0);
    const theaterInfo = await fetch(
      `./api/theaters?title=${movieTitle}&lat=${location.lat}&lng=${location.lng}&country=${location.country}&city=${location.value}`,
    ).then((res) => res.json());
    setTheaterList(theaterInfo.theaterList);
    setTheaterLocationList(theaterInfo.theaterLocationList);
    setTheaterDate(theaterInfo.theaterList[0]?.date ?? "");
    setNextMarkerPositions(theaterInfo.theaterLocationList[0]?.locationList ?? []);
    setPreMarkerPositions(theaterInfo.theaterLocationList[0]?.locationList ?? []);
  };

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(async (position) => {
      const lat = String(position.coords.latitude); // 緯度
      const lng = String(position.coords.longitude); // 経度
      // 緯度・経度を住所に変換する
      const geocodeAddressResponse = await fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${googleMapApiKey}`,
      );
      const geocodeAddressData = await geocodeAddressResponse.json();

      // 都市名（例：Tokyo）を取得する
      const city = geocodeAddressData?.results[0]?.address_components.find((area: any) => {
        return area.types.includes("administrative_area_level_1");
      }).long_name;
      const value =
        (area.find((info) => {
          return info.label == city;
        })?.value ??
          "") ||
        (area.find((info) => {
          return info.value == city;
        })?.label ??
          "");

      // 国名の略（例：JP）を取得する
      const country = geocodeAddressData?.results[0]?.address_components.find((area: any) => {
        return area.types.includes("country");
      }).short_name;

      setCurrentLocation({
        value: value,
        label: "近く",
        lat: lat,
        lng: lng,
        country: country,
      });
      setIsisLoading(false);
    });
  }, [googleMapApiKey, movieTitle]);

  // 選択された日付の映画館リスト
  const targetTheaterList =
    theaterList.find((theaterInfo) => {
      return theaterInfo.date === theaterDate;
    })?.theaters ?? [];

  // 現在と次に表示するGoogleマップのピン全体のリスト
  const preNextPinList = Array.from(
    new Set([...preMarkerPositions, ...nextMarkerPositions].map((item) => JSON.stringify(item))),
    (jsonString) => JSON.parse(jsonString),
  );

  const addKeywordToLink = (link: string, keyword: string) => {
    // originalLinkからクエリパラメータの部分を抽出
    const queryStringIndex = link.indexOf("?");
    const queryParameters = link.substring(queryStringIndex + 1);

    // クエリパラメータ文字列をオブジェクトに変換
    const queryParams = new URLSearchParams(queryParameters);

    // 'q'パラメータの値を取得してデコード
    const qValue = queryParams.get("q") || "";
    const decodedQValue = decodeURIComponent(qValue);

    // movieTitleをスペースで繋げる
    const newQValue = `${keyword} ${decodedQValue} 周辺 上映時間`;

    // 新しいURLを生成
    const newURL = link.substring(0, queryStringIndex + 1) + "q=" + encodeURIComponent(newQValue);

    return newURL;
  };

  return (
    <>
      <Head>
        <title>映画GO</title>
      </Head>
      <div className={styles["body"]}>
        <div className={styles["content"]}>
          <Header session={session} />
          <div className={styles["main"]}>
            <div className={styles["subject"]}>
              <h2>映画館一覧</h2>
              <p>{`"${movieTitle}" をみにいく`}</p>
            </div>
            <div className={styles["theater-wrapper"]}>
              <div className={styles["theater-left"]}>
                <div className={styles["search-theaters-wrapper"]}>
                  {/* <div className={styles["near-theaters-wrapper"]}>
                    <div
                      className={`${styles["near-theaters-btn"]} ${
                        Object.keys(selectedArea).length > 0 ? "" : styles.active
                      }`}
                      onClick={() => {
                        searchWithCurrentLocation(currentLocation);
                      }}
                    >
                      <p>近くで探す</p>
                    </div>
                  </div> */}

                  <div
                    className={`${styles["area-search-wrapper"]} ${
                      Object.keys(selectedArea).length > 0 ? styles.active : ""
                    }`}
                  >
                    <p>エリアで探す：</p>
                    <Select
                      className={styles["select-area"]}
                      name="area"
                      value={Object.keys(selectedArea).length > 0 ? selectedArea : null}
                      options={area}
                      defaultValue={Object.keys(selectedArea).length > 0 ? selectedArea : null}
                      onChange={(value) => {
                        searchWithArea(value);
                      }}
                    />
                  </div>
                </div>
                {isLoading ? (
                  <div className={styles["loading-container"]}>
                    <div
                      className={`${styles["loading-indicator"]} ${styles["loading-animation"]}`}
                    ></div>
                    <div className={styles["loading-text"]}>映画館情報を取得中...</div>
                  </div>
                ) : (
                  <>
                    <p className={styles["near-theaters"]}>
                      {Object.keys(selectedArea).length > 0 ? (
                        <>{`"${selectedArea.label}"の映画館を表示中`}</>
                      ) : (
                        <>{`"近く"の映画館を表示中`}</>
                      )}
                      <span>{`${targetTheaterList.length}  件`}</span>
                    </p>
                    {theaterList.length > 0 ? (
                      <>
                        <ul className={styles["date-menu"]}>
                          {theaterList.map((theater, i) => {
                            return (
                              <li
                                key={i}
                                className={`${styles.date} ${
                                  i === activeDateIndex ? styles.active : ""
                                }`}
                                onClick={() => {
                                  handleTheaterDate(theater.date);
                                  handleActive(i);
                                  handlePositions(
                                    theaterLocationList.find((theaterLocationInfo) => {
                                      return theaterLocationInfo.date === theater.date;
                                    }).locationList,
                                  );
                                }}
                              >
                                {theater.day === "今日" ? (
                                  <>{theater.day}</>
                                ) : (
                                  <>{`${theater.date}(${theater.day})`}</>
                                )}
                              </li>
                            );
                          })}
                        </ul>
                        <div className={styles["theaters"]}>
                          {targetTheaterList.map((theater, i) => {
                            return (
                              <Link
                                key={i}
                                href={addKeywordToLink(theater.link, movieTitle)}
                                style={{ borderBottom: "0.5px solid #b8c1ec", display: "block" }}
                              >
                                <div className={styles["theater"]}>
                                  <span className={styles["theater-number"]}>{`${i + 1}`}</span>
                                  <span
                                    className={styles["theater-name"]}
                                  >{`　${theater.name}`}</span>
                                  {theater.showing.map((show, j) => {
                                    return (
                                      <div key={`${i}${j}`} className={styles["theater-detail"]}>
                                        <div className={styles["theater-type"]}>{show.type}</div>
                                        <div className={styles["theater-time"]}>
                                          {show.time.map((time, k) => {
                                            return <div key={`${i}${j}${k}`}>{time}</div>;
                                          })}
                                        </div>
                                      </div>
                                    );
                                  })}
                                </div>
                              </Link>
                            );
                          })}
                        </div>
                      </>
                    ) : (
                      <div className={styles["error-message"]}>
                        {/* <p>映画館情報を取得できませんでした。</p> */}
                        <p>検索回数上限を超過しました。しばらくしてから再度お試しください</p>
                      </div>
                    )}
                  </>
                )}
              </div>
              <div className={styles["theater-right"]}>
                <Wrapper apiKey={googleMapApiKey}>
                  <Map selectedArea={selectedArea} currentLocation={currentLocation}>
                    {(map) => (
                      <>
                        {preNextPinList.map((position: any) => (
                          <Marker
                            key={JSON.stringify(position)}
                            position={position}
                            nextMarkerPositions={nextMarkerPositions}
                            map={map}
                          />
                        ))}
                      </>
                    )}
                  </Map>
                </Wrapper>
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const googleMapApiKey = process.env.GOOGLE_MAPS_API_KEY;
  const movieTitle = context.query.title;
  const lat = context.query.lat;
  const lng = context.query.lng;
  const city = context.query.city;
  const country = context.query.country;
  const resData = await fetch(
    `http://localhost:3000/api/theaters?title=${movieTitle}&lat=${lat}&lng=${lng}&country=${country}&city=${city}`,
  );
  const theaterInfo = await resData.json();

  return {
    props: {
      initialTheaterList: theaterInfo.theaterList,
      initialTheaterLocationList: theaterInfo.theaterLocationList,
      movieTitle,
      googleMapApiKey,
    },
  };
};

export default Theaters;
