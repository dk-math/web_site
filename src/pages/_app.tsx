import { MantineProvider } from "@mantine/core";
import type { AppProps } from "next/app";
import { Session } from "next-auth";
import { SessionProvider } from "next-auth/react";
import React from "react";
import "@aws-amplify/ui-react/styles.css";
import "../styles/globals.css";
import "@mantine/core/styles.css";

export default function App({ Component, pageProps }: AppProps<{ session: Session }>) {
  return (
    <SessionProvider session={pageProps.session}>
      <MantineProvider>
        <Component {...pageProps} />
      </MantineProvider>
    </SessionProvider>
  );
}
