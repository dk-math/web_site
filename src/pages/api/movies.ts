// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  res.setHeader("Access-Control-Allow-Origin", "https://eigago.com");
  res.setHeader("Access-Control-Allow-Methods", "GET");
  const keyword = req.query.keyword as string;
  const page = req.query.page as string;
  const movieApiKey = process.env.TMDB_API_KEY;
  const resData = await fetch(
    `https://api.themoviedb.org/3/search/movie?api_key=${movieApiKey}&language=ja&region=JP&query=${keyword}&page=${page}`,
  );
  const movieList = (await resData.json()).results;
  movieList.sort((a: any, b: any) => (a.release_date < b.release_date ? 1 : -1));
  res.status(200).json(movieList);
};

export default handler;
