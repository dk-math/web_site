// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const id = req.query.id;
  const movieApiKey = process.env.TMDB_API_KEY;
  const [movie, credit] = await Promise.all([
    fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${movieApiKey}&language=ja`).then(
      (res) => res.json(),
    ),
    fetch(
      `https://api.themoviedb.org/3/movie/${id}/credits?api_key=${movieApiKey}&language=ja`,
    ).then((res) => res.json()),
  ]);

  // 日本語版の概要が存在しない場合に他言語版の概要で上書きする
  let overwriteMovie;
  if (!movie.overview) {
    overwriteMovie = await fetch(
      `https://api.themoviedb.org/3/movie/${id}?api_key=${movieApiKey}`,
    ).then((res) => res.json());
    const overwriteMovieOverview = overwriteMovie.overview;
    movie.overview = overwriteMovieOverview || "";
  }

  const directorList = credit.crew
    .filter((crew: any) => crew.job === "Director")
    .map(async (director: any) => {
      const [directorInfo, directorMovies] = await Promise.all([
        fetch(
          `https://api.themoviedb.org/3/person/${director.id}?api_key=${movieApiKey}&language=ja`,
        ).then((res) => res.json()),
        fetch(
          `https://api.themoviedb.org/3/person/${director.id}/movie_credits?api_key=${movieApiKey}&language=ja`,
        ).then((res) => res.json()),
      ]);
      const directorMoviesFiltered = directorMovies.crew.filter(
        (movie: any) =>
          movie.release_date !== "" &&
          movie.job === "Director" &&
          movie.title !== undefined &&
          movie.poster_path !== null,
      );
      return {
        id: director.id,
        name: director.name,
        info: directorInfo,
        movies: directorMoviesFiltered,
      };
    });

  res
    .status(200)
    .json({ movie: movie, credit: credit, directorList: await Promise.all(directorList) });
};

export default handler;
