import * as deepl from "deepl-node";
import type { NextApiRequest, NextApiResponse } from "next";
import OpenAI from "openai";
import { getRandomTitleLang } from "@/src/util/ramdom";

type MovieInfo =
  | {
      title: string;
      tagline: string;
      overview: string;
    }
  | undefined;

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const title1 = req.query.title1 as string;
  const title2 = req.query.title2 as string;
  const overview1 = req.query.overview1 as string;
  const overview2 = req.query.overview2 as string;
  const openaiApiKey = process.env.OPENAI_API_KEY as string;
  const deeplApiKey = process.env.DEEPL_API_KEY as string;
  const translator = new deepl.Translator(deeplApiKey);
  const openai = new OpenAI({
    apiKey: openaiApiKey,
  });

  // 英語でchatgptにリクエストすることでトークンを節約する
  const translatedOverview1 = (await translator.translateText(overview1, null, "en-US")).text;
  const translatedOverview2 = (await translator.translateText(overview2, null, "en-US")).text;
  const translatedChatgptRequest = `Create a new movie based on movie summaries 1 and 2. Please answer in JSON by filling in "answer" in {title: "answer", tagline: "answer", overview: "answer"}.
  . 1.${translatedOverview1}2.${translatedOverview2}`;
  const response = await openai.chat.completions.create({
    model: "gpt-3.5-turbo",
    response_format: { type: "json_object" },
    messages: [
      {
        role: "system",
        content: "You are a helpful assistant designed to output JSON.",
      },
      {
        role: "user",
        content: translatedChatgptRequest,
      },
    ],
    temperature: 0.7,
    top_p: 1,
  });
  let chatgptAnswer = "";
  if (response.choices[0].message.content !== null) {
    chatgptAnswer = response.choices[0].message.content;
  }
  const resultObject = JSON.parse(chatgptAnswer) as MovieInfo;
  const movieTitle = resultObject?.title as string;
  const movieTagline = resultObject?.tagline as string;
  const movieOverview = resultObject?.overview as string;

  let movieImageUrl;
  if (movieTitle && movieTagline && movieOverview) {
    const imageInfo = await openai.images.generate({
      model: "dall-e-2",
      prompt: `
      Movie poster with title: ${movieTitle}, tagline: ${movieTagline}`,
      n: 1,
      response_format: "url",
      size: "512x512",
    });
    movieImageUrl = imageInfo.data[0].url;
  }

  // 映画のタイトルは日本語か英語をランダムで指定;
  const titleLang = getRandomTitleLang();
  const translatedMovieTitle = (
    await translator.translateText(movieTitle, null, titleLang as "ja" | "en-US")
  ).text;
  const translatedMovieTagline = (await translator.translateText(movieTagline, null, "ja")).text;
  const translatedMovieOverview = (await translator.translateText(movieOverview, null, "ja")).text;

  res.status(200).json({
    translatedMovieTitle,
    translatedMovieTagline,
    translatedMovieOverview,
    movieImageUrl,
    materialTitle1: title1,
    materialTitle2: title2,
  });
};

export default handler;
