// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import type { GoogleParameters } from "serpapi";
import { getJson } from "serpapi";
import area from "../../data/area";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const theaterApiKey = process.env.GOOGLE_SERP_API_KEY as string;
  const googleMapApiKey = process.env.GOOGLE_MAPS_API_KEY as string;
  const title = req.query.title as string;
  // const areaInfo = await getCurrentLocation(googleMapApiKey);
  const params = {
    q: `${title} 映画館 ${req.query.city}`,
    location: req.query.city as string,
    hl: "",
    gl: req.query.country as string,
    api_key: theaterApiKey,
  } satisfies GoogleParameters;
  // const resData = await getJson("google", params);

  const theaterList = [] as any[];
  // (resData.showtimes ?? []).map((theaterInfo: any) => {
  //   return {
  //     day: splitDate(theaterInfo.day).day,
  //     date: splitDate(theaterInfo.day).date,
  //     theaters: theaterInfo.theaters,
  //   };
  // }) ?? [];

  const theaterLocationList = await getTheaterAddressToLatLng(theaterList, googleMapApiKey);

  res.status(200).json({
    theaterList: theaterList,
    theaterLocationList: theaterLocationList,
  });
};

const getTheaterAddressToLatLng = async (theaterData: any[], apiKey: string) => {
  return await Promise.all(
    theaterData.map(async (info: any) => {
      return {
        date: info.date,
        locationList: await Promise.all(
          info.theaters.map(async (theater: any) => {
            const geocodeLngLatResponse = await fetch(
              `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(
                theater.address,
              )}&key=${apiKey}`,
            );
            const geocodeLngLatData = await geocodeLngLatResponse.json();
            return geocodeLngLatData?.results[0]?.geometry?.location ?? [];
          }),
        ),
      };
    }),
  );
};

// 正規表現を使って文字列を分割する関数
const splitDate = (input: string): { day: string; date: string } => {
  if (typeof input !== "string") {
    // inputが文字列でない場合の処理
    return { day: "", date: "" };
  }
  const matchResult = input.match(/([今日日月火水木金土]+)(\d+月\d+日)/);
  if (!matchResult) {
    // パターンにマッチしない場合は、デフォルトの値を返すなどの処理を行うことができます。
    return { day: "", date: "" };
  }
  const dayPart = matchResult[1]; // "今日" または "日" または "月"
  const datePart = matchResult[2]; // "2月3日" など
  return { day: dayPart, date: datePart };
};

export default handler;
