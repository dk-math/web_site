// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const movieApiKey = process.env.TMDB_API_KEY;
  const movieMaxPage = 20;
  const movieList = (
    await Promise.all(
      Array.from({ length: movieMaxPage }, (_, i) =>
        fetch(
          `https://api.themoviedb.org/3/movie/now_playing?api_key=${movieApiKey}&language=ja&page=${
            i + 1
          }`,
        ).then((resData) => resData.json()),
      ),
    )
  ).flatMap(({ results }) => results);
  res.status(200).json(movieList);
};

export default handler;
