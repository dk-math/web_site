import { GetServerSideProps } from "next";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useSession } from "next-auth/react";
import React, { useState } from "react";
import noimage from "../../public/noimage.jpg";
import FavoriteButton from "../components/FavoriteButton";
import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import { HomePageModal } from "../components/HomePageModal";
import styles from "../styles/detail.module.css";
import { getScoreColorClass, getScoreBarLength } from "../util/scoreGraph";

type Props = {
  movieDetail: any;
};

const Detail: React.FC<Props> = ({ movieDetail }) => {
  const { data: session } = useSession();
  const hour = Math.floor(movieDetail.movie.runtime / 60);
  const minute = movieDetail.movie.runtime % 60;
  const scoreColorClass = getScoreColorClass(movieDetail.movie.vote_average);
  const scoreBarLength = getScoreBarLength(movieDetail.movie.vote_average);
  const [homePageModalFlg, setHomePageModalFlg] = useState(false);
  const handleCloseModal = () => {
    setHomePageModalFlg(false);
  };

  return (
    <>
      <Head>
        <title>映画GO</title>
      </Head>
      <div className={styles["body"]}>
        <div className={styles["content"]}>
          <Header session={session} />
          <div className={styles["main"]}>
            <div className={styles["subject"]}>
              <h2>作品紹介</h2>
            </div>
            <div className={styles["detail"]}>
              <div className={styles["detail-left"]}>
                <div className={styles["movie-poster-wrapper"]}>
                  <FavoriteButton movie={movieDetail.movie} />
                  <div className={styles["movie-poster"]}>
                    <Image
                      className={styles["movie-poster-img"]}
                      src={
                        movieDetail.movie.poster_path
                          ? `https://image.tmdb.org/t/p/w500${movieDetail.movie.poster_path}`
                          : noimage
                      }
                      width={500}
                      height={500}
                      alt={"Movie Poster"}
                    />
                  </div>
                  <Link
                    href={`/theaters?title=${movieDetail.movie.title}&lat=35.689185&lng=139.691648&country=JP&city=tokyo`}
                  >
                    <div className={styles["theater-btn"]}>映画館へ</div>
                  </Link>
                </div>
              </div>
              <div className={styles["detail-right"]}>
                <div className={styles["movie-title-wrapper"]}>
                  <h2 className={styles["movie-title"]}>{movieDetail.movie.title}</h2>
                  {movieDetail.movie.homepage && (
                    <a
                      className={styles["movie-homepage-link"]}
                      onClick={() => setHomePageModalFlg(true)}
                    >
                      ホームページ &gt;&gt;
                    </a>
                  )}
                </div>
                <p className={styles["movie-tagline"]}>{movieDetail.movie.tagline}</p>
                <p className={styles["movie-release-date"]}>
                  {movieDetail.movie.release_date}
                  <span className={styles["movie-runtime"]}>{`${hour}時間 ${minute}分`}</span>
                </p>
                <div className={styles["movie-score"]}>
                  <div id="pie-chart">
                    <div className={styles["pie-chart-wrap"]}>
                      <div className={`${styles["box"]} ${styles[`${scoreColorClass}`]}`}>
                        <div className={styles["percent"]}>
                          <svg>
                            <circle className={styles["base"]} cx="75" cy="75" r="50"></circle>
                            <circle
                              className={styles["line"]}
                              style={scoreBarLength}
                              cx="75"
                              cy="75"
                              r="50"
                            ></circle>
                          </svg>
                          <div className={styles["number"]}>
                            <h3 className={styles["title"]}>
                              {Math.floor(movieDetail.movie.vote_average * 10)}
                              <span>%</span>
                            </h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p>ユーザースコア</p>
                </div>

                {movieDetail.movie.overview && (
                  <>
                    <p className={styles["movie-overview-title"]}>概要</p>
                    <p className={styles["movie-overview"]}>{movieDetail.movie.overview}</p>
                  </>
                )}
              </div>
            </div>
            {movieDetail.directorList.map((director: any, i: number) => {
              return (
                <div key={i} className={styles["director-box"]}>
                  <h2>監督</h2>
                  <div className={styles["director-profile"]}>
                    <div className={styles["director-left"]}>
                      <Image
                        className={styles["director-img"]}
                        src={
                          director.info.profile_path
                            ? `https://image.tmdb.org/t/p/w500${director.info.profile_path}`
                            : noimage
                        }
                        alt="Director's Image"
                        width={500}
                        height={500}
                      />
                    </div>
                    <div className={styles["director-right"]}>
                      <h2>{director.info.name}</h2>
                      <p>{director.info.birthday}</p>
                      <h3 className={styles["director-movie-subject"]}>{"作品"}</h3>
                      <div className={styles["director-movies"]}>
                        {director.movies.map((movie: any, i: number) => {
                          return (
                            <Link key={i} href={`/detail?id=${movie.id}`}>
                              <div className={styles["director-movie"]}>
                                <div className={styles["director-movie-poster"]}>
                                  <Image
                                    className={styles["director-movie-img"]}
                                    src={
                                      movie.poster_path
                                        ? `https://image.tmdb.org/t/p/w500${movie.poster_path}`
                                        : noimage
                                    }
                                    alt="Director's Movie Image"
                                    width={500}
                                    height={500}
                                  />
                                </div>
                                <p className={styles["director-movie-title"]}>{movie.title}</p>
                              </div>
                            </Link>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
            {movieDetail.credit.cast.length > 0 && (
              <div className={styles["cast-box"]}>
                <h2>出演者</h2>
                <div className={styles["casts"]}>
                  {movieDetail.credit.cast.map((cast: any, i: number) => {
                    return (
                      <div key={i} className={styles["cast"]}>
                        <Image
                          className={styles["cast-img"]}
                          src={
                            cast.profile_path
                              ? `https://image.tmdb.org/t/p/w500${cast.profile_path}`
                              : noimage
                          }
                          alt="Cast's Movie Image"
                          width={500}
                          height={500}
                        />
                        <p className={styles["cast-name"]}>{cast.name}</p>
                      </div>
                    );
                  })}
                </div>
              </div>
            )}
            {homePageModalFlg && (
              <HomePageModal movieDetail={movieDetail} onClose={handleCloseModal} />
            )}
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const resData = await fetch(`http://localhost:3000/api/detail?id=${context.query.id}`);
  const movieDetail = await resData.json();
  return { props: { movieDetail } };
};

export default Detail;
