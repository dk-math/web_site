import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useSession } from "next-auth/react";
import React, { useEffect, useState } from "react";
import noimage from "../../public/noimage.jpg";
import FavoriteButton from "../components/FavoriteButton";
import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import styles from "../styles/movies.module.css";

type Props = {};

const Favorites: React.FC<Props> = () => {
  const { data: session } = useSession();
  const [favoriteMovies, setFavoriteMovies] = useState<any[]>([]);

  useEffect(() => {
    // localStorageからお気に入り映画を取得
    const favorites = JSON.parse(localStorage.getItem("favorites") || "[]");
    setFavoriteMovies(favorites);
  }, []);

  return (
    <>
      <Head>
        <title>お気に入り</title>
      </Head>
      <div className={styles["body"]}>
        <div className={styles["content"]}>
          <Header session={session} />
          <div className={styles["main"]}>
            <div className={styles["subject"]}>
              <h2>お気に入り</h2>
            </div>
            <ul className={styles["movies"]}>
              {favoriteMovies.map((movie: any, i: number) => {
                return (
                  <li key={i} className={styles["movie"]}>
                    <div className={styles["movie-poster-wrapper"]}>
                      <FavoriteButton movie={movie} />
                      <Link href={`/detail?id=${movie.id}`}>
                        <div className={styles["movie-poster"]}>
                          <Image
                            className={styles["movie-poster-img"]}
                            src={
                              movie.poster_path
                                ? `https://image.tmdb.org/t/p/w500${movie.poster_path}`
                                : noimage
                            }
                            alt={"Movie Poster"}
                            width={500}
                            height={500}
                          />
                        </div>
                      </Link>
                      <Link
                        href={`/theaters?title=${movie.title}&lat=35.689185&lng=139.691648&country=JP&city=tokyo`}
                      >
                        <div className={styles["theater-btn"]}>映画館へ</div>
                      </Link>
                      <div className={styles["movie-title"]}>
                        <p>
                          <Link href={`/detail?id=${movie.id}`}>{movie.title}</Link>
                        </p>
                      </div>
                      <div className={styles["movie-release-date"]}>
                        <p>{movie.release_date}</p>
                      </div>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export default Favorites;
