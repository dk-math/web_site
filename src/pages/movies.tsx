import { GetServerSideProps } from "next";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useSession } from "next-auth/react";
import React, { useState } from "react";
import noimage from "../../public/noimage.jpg";
import FavoriteButton from "../components/FavoriteButton";
import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import styles from "../styles/movies.module.css";

type Props = {
  initialMovieList: any[];
  initialNextMovieList: any[];
  initialDisplayNextPageButtonFlag: boolean;
  keyword: string;
};

const Movies: React.FC<Props> = ({
  initialMovieList,
  initialNextMovieList,
  initialDisplayNextPageButtonFlag,
  keyword,
}) => {
  const { data: session } = useSession();
  const [movieList, setMovieList] = useState(initialMovieList);
  const [nextMovieList, setNextMovieList] = useState(initialNextMovieList);
  const [page, setPage] = useState(1);
  const [displayNextPageButtonFlag, setDisplayNextPageButtonFlag] = useState(
    initialDisplayNextPageButtonFlag,
  );

  const loadNextPage = async () => {
    try {
      const nextPage = page + 1;
      setMovieList((prevMovieList) => [...prevMovieList, ...nextMovieList]);
      const [newMovieList] = await Promise.all([
        fetch(`./api/movies?keyword=${keyword}&page=${nextPage + 1}`).then((res) => res.json()),
      ]);
      if (newMovieList.length === 0) {
        setDisplayNextPageButtonFlag(false);
      }
      setPage(nextPage);
      setNextMovieList(newMovieList);
    } catch (error) {
      console.error("映画情報の読み込みに失敗しました");
    }
  };

  return (
    <>
      <Head>
        <title>映画GO</title>
      </Head>
      <div className={styles["body"]}>
        <div className={styles["content"]}>
          <Header session={session} />
          <div className={styles["main"]}>
            <div className={styles["subject"]}>
              <h2>作品一覧</h2>
              <p>{`"${keyword}"の検索結果を表示中`}</p>
            </div>
            <ul className={styles["movies"]}>
              {movieList.map((movie, i) => {
                return (
                  <li key={i} className={styles["movie"]}>
                    <div className={styles["movie-poster-wrapper"]}>
                      <FavoriteButton movie={movie} />
                      <Link href={`/detail?id=${movie.id}`}>
                        <div className={styles["movie-poster"]}>
                          <Image
                            className={styles["movie-poster-img"]}
                            src={
                              movie.poster_path
                                ? `https://image.tmdb.org/t/p/w500${movie.poster_path}`
                                : noimage
                            }
                            alt={"Movie Poster"}
                            width={500}
                            height={500}
                          />
                        </div>
                      </Link>
                      <Link
                        href={`/theaters?title=${movie.title}&lat=35.689185&lng=139.691648&country=JP&city=tokyo`}
                      >
                        <div className={styles["theater-btn"]}>映画館へ</div>
                      </Link>
                      <div className={styles["movie-title"]}>
                        <p>
                          <Link href={`/detail?id=${movie.id}`}>{movie.title}</Link>
                        </p>
                      </div>
                      <div className={styles["movie-release-date"]}>
                        <p>{movie.release_date}</p>
                      </div>
                    </div>
                  </li>
                );
              })}
            </ul>
            {displayNextPageButtonFlag && (
              <div className={styles["more-movie-button"]}>
                <a onClick={loadNextPage}>もっとみる</a>
              </div>
            )}
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const [initialMovieList, initialNextMovieList] = await Promise.all([
    fetch(`http://localhost:3000/api/movies?keyword=${context.query.keyword}&page=${1}`).then(
      (res) => res.json(),
    ),
    fetch(`http://localhost:3000/api/movies?keyword=${context.query.keyword}&page=${2}`).then(
      (res) => res.json(),
    ),
  ]);
  const initialDisplayNextPageButtonFlag = initialNextMovieList.length > 0;
  const keyword = context.query.keyword;
  return {
    props: {
      initialMovieList,
      initialNextMovieList,
      initialDisplayNextPageButtonFlag,
      keyword,
    },
  };
};

export default Movies;
