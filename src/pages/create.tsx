import html2canvas from "html2canvas"; // html2canvasを追加
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useSession } from "next-auth/react";
import React, { useEffect, useState } from "react";
import noimage from "../../public/noimage.jpg";
import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import styles from "../styles/create.module.css";
import { getRandomInt, getRandomFloat, getRandomDate } from "../util/ramdom";
import { getScoreColorClass, getScoreBarLength } from "../util/scoreGraph";

type Props = {};

const Create: React.FC<Props> = () => {
  const { data: session } = useSession();
  const [favoriteMovies, setFavoriteMovies] = useState<any[]>([]);
  const upperSelectionLimit = 2;
  const [selectedMovies, setSelectedMovies] = useState<any[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [movieTitle, setMovieTitle] = useState("");
  const [movieTagline, setMovieTagline] = useState("");
  const [movieOverview, setMovieOverview] = useState("");
  const [movieImageUrl, setMovieImageUrl] = useState("");
  const [movieMaterialTitle1, setMovieMaterialTitle1] = useState("");
  const [movieMaterialTitle2, setMovieMaterialTitle2] = useState("");
  const [movieReleaseDate, setMovieReleaseDate] = useState("");
  const [movieRuntime, setMovieRuntime] = useState(0);
  const [movieScore, setMovieScore] = useState(0);
  const [isButtonOn, setIsButtonOn] = useState(false);

  useEffect(() => {
    // localStorageからお気に入り映画を取得
    const favorites = JSON.parse(localStorage.getItem("favorites") || "[]");
    setFavoriteMovies(favorites);
  }, []);

  const handleSelectMovie = (movie: any) => {
    // 映画がすでに選択されているかどうかを確認
    const isSelected = selectedMovies.some((selectedMovie) => selectedMovie.id === movie.id);
    // 選択された映画の数が上限未満、かつ選択されていない映画をクリックした場合
    if (selectedMovies.length < upperSelectionLimit && !isSelected) {
      // 選択されていない場合は追加し、選択されている場合は削除する
      setSelectedMovies((prevSelectedMovies) => {
        if (!isSelected) {
          return [...prevSelectedMovies, movie];
        } else {
          return prevSelectedMovies.filter((selectedMovie) => selectedMovie.id !== movie.id);
        }
      });
    } else if (isSelected) {
      // すでに選択された映画の選択ボタンをクリックした場合は、選択解除する
      setSelectedMovies(selectedMovies.filter((selectedMovie) => selectedMovie.id !== movie.id));
    }
  };

  const handleClearSelection = () => {
    setSelectedMovies([]);
  };

  const scrollToBottom = () => {
    setTimeout(() => {
      window.scrollTo({
        top: document.body.scrollHeight,
        behavior: "smooth",
      });
    }, 100); // ボタン押下時に非表示になっているコンポーネントがあるため、100ミリ秒の遅延を設ける
  };

  const createNewMovie = async () => {
    try {
      setIsLoading(true); // レスポンスを待っている間はローディング状態をtrueに設定
      const chatgptAns = await fetch(
        `./api/create?title1=${selectedMovies[0]?.title}&title2=${selectedMovies[1]?.title}&overview1=${selectedMovies[0]?.overview}&overview2=${selectedMovies[1]?.overview}`,
      ).then((res) => res.json());
      setMovieTitle(chatgptAns.translatedMovieTitle);
      setMovieTagline(chatgptAns.translatedMovieTagline);
      setMovieOverview(chatgptAns.translatedMovieOverview);
      setMovieImageUrl(chatgptAns.movieImageUrl);
      setMovieMaterialTitle1(chatgptAns.materialTitle1);
      setMovieMaterialTitle2(chatgptAns.materialTitle2);
      setMovieReleaseDate(getRandomDate());
      setMovieRuntime(getRandomInt(30, 300));
      setMovieScore(getRandomFloat(0, 10, 1));
    } catch (error) {
      console.error("chatgptの回答取得に失敗しました");
    } finally {
      setIsLoading(false); // レスポンス後、エラーが発生した後はローディング状態をfalseに設定
    }
  };

  const hour = Math.floor(movieRuntime / 60);
  const minute = movieRuntime % 60;
  const scoreColorClass = getScoreColorClass(movieScore);
  const scoreBarLength = getScoreBarLength(movieScore);

  const createMovieWrapperClassName = isLoading
    ? `${styles["create-movie-wrapper"]} ${styles["animate-bg"]}`
    : `${styles["create-movie-wrapper"]} ${styles["result-bg"]}`;

  const handleDownloadImage = () => {
    const detailElement = document.getElementById("create-movie-wrapper") as HTMLElement; // HTMLElementにキャスト
    console.log(detailElement);
    if (detailElement) {
      html2canvas(detailElement).then((canvas) => {
        const image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        const link = document.createElement("a");
        link.href = image;
        link.download = "movie_detail.jpeg";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      });
    }
  };

  return (
    <>
      <Head>
        <title>映画生成AI</title>
      </Head>
      <div className={styles["body"]}>
        <div className={styles["content"]}>
          <Header session={session} />
          <div className={styles["main"]}>
            <div className={styles["subject"]}>
              <h2>映画生成AI</h2>
              <p>
                素材にするお気に入り映画を<span>&nbsp;{upperSelectionLimit}つ&nbsp;</span>
                選んでください
              </p>
              <button className={styles["clear-selection-button"]} onClick={handleClearSelection}>
                選択解除
              </button>
            </div>
            <ul className={styles["movies"]}>
              {favoriteMovies.map((movie: any, i: number) => {
                return (
                  <li key={i} className={styles["movie"]}>
                    <div className={styles["movie-poster-fav-wrapper"]}>
                      <div className={styles["select-button"]}>
                        <label className={styles["select-checkbox"]}>
                          <input
                            type="checkbox"
                            onChange={() => handleSelectMovie(movie)}
                            checked={selectedMovies.some(
                              (selectedMovie) => selectedMovie.id === movie.id,
                            )}
                          />
                          <span className={styles["checkmark"]}></span>
                        </label>
                      </div>
                      <Link href={`/detail?id=${movie.id}`}>
                        <div className={styles["movie-poster-fav"]}>
                          <Image
                            className={styles["movie-poster-fav-img"]}
                            src={
                              movie.poster_path
                                ? `https://image.tmdb.org/t/p/w500${movie.poster_path}`
                                : noimage
                            }
                            alt={"Movie Poster"}
                            width={500}
                            height={500}
                          />
                        </div>
                      </Link>
                      <Link
                        href={`/theaters?title=${movie.title}&lat=35.689185&lng=139.691648&country=JP&city=tokyo`}
                      >
                        <div className={styles["theater-btn"]}>映画館へ</div>
                      </Link>
                      <div className={styles["movie-title"]}>
                        <p>
                          <Link href={`/detail?id=${movie.id}`}>{movie.title}</Link>
                        </p>
                      </div>
                      <div className={styles["movie-release-date"]}>
                        <p>{movie.release_date}</p>
                      </div>
                    </div>
                  </li>
                );
              })}
            </ul>
            <button
              className={styles["create-movie-button"]}
              onClick={() => {
                scrollToBottom();
                createNewMovie();
                setIsButtonOn(true);
              }}
              disabled={selectedMovies.length < upperSelectionLimit}
            >
              映画を生成する
            </button>
            {isButtonOn && (
              <>
                {isLoading ? (
                  <div id={"create-movie-wrapper"} className={createMovieWrapperClassName}>
                    <div className={styles["loading-container"]}>
                      <div
                        className={`${styles["loading-indicator"]} ${styles["loading-animation"]}`}
                      ></div>
                      <div className={styles["loading-text"]}>AIが映画を生成中...</div>
                    </div>
                  </div>
                ) : movieTitle && movieTagline && movieOverview ? (
                  <>
                    <div id={"create-movie-wrapper"} className={createMovieWrapperClassName}>
                      <div className={styles["detail"]}>
                        <div className={styles["detail-left"]}>
                          <div className={styles["movie-poster-wrapper"]}>
                            <div className={styles["movie-poster"]}>
                              <Image
                                className={styles["movie-poster-img"]}
                                src={movieImageUrl ? movieImageUrl : noimage}
                                alt={"Movie Poster"}
                                width={500}
                                height={500}
                              />
                            </div>
                          </div>
                        </div>
                        <div className={styles["detail-right"]}>
                          <div className={styles["movie-title-wrapper"]}>
                            <h2 className={styles["movie-title"]}>{movieTitle}</h2>
                          </div>
                          <p className={styles["movie-tagline"]}>{movieTagline}</p>
                          <p className={styles["movie-release-date"]}>
                            {movieReleaseDate}
                            <span
                              className={styles["movie-runtime"]}
                            >{`${hour}時間 ${minute}分`}</span>
                          </p>
                          <div className={styles["movie-score"]}>
                            <div id="pie-chart">
                              <div className={styles["pie-chart-wrap"]}>
                                <div className={`${styles["box"]} ${styles[`${scoreColorClass}`]}`}>
                                  <div className={styles["percent"]}>
                                    <svg>
                                      <circle
                                        className={styles["base"]}
                                        cx="75"
                                        cy="75"
                                        r="50"
                                      ></circle>
                                      <circle
                                        className={styles["line"]}
                                        style={scoreBarLength}
                                        cx="75"
                                        cy="75"
                                        r="50"
                                      ></circle>
                                    </svg>
                                    <div className={styles["number"]}>
                                      <h3 className={styles["title"]}>
                                        {Math.floor(movieScore * 10)}
                                        <span>%</span>
                                      </h3>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <p>ユーザースコア</p>
                          </div>
                          {movieOverview !== "" && (
                            <>
                              <p className={styles["movie-overview-title"]}>概要</p>
                              <p className={styles["movie-overview"]}>{movieOverview}</p>
                            </>
                          )}
                          <div className={styles["movie-material"]}>
                            <p>{`"${movieMaterialTitle1}" と "${movieMaterialTitle2}"を元にした映画`}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button className={styles["download-button"]} onClick={handleDownloadImage}>
                      映画画像をダウンロード
                    </button>
                  </>
                ) : (
                  <div className={styles["error-message"]}>
                    <p>映画の生成に失敗しました。もう一度お試しください</p>
                  </div>
                )}
              </>
            )}
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export default Create;
