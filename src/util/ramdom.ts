export const getRandomInt = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const getRandomFloat = (min: number, max: number, precision: number): number => {
  const random = Math.random() * (max - min) + min;
  return parseFloat(random.toFixed(precision));
};

export const getRandomDate = (): string => {
  const startDate: Date = new Date(1980, 0, 1); // 開始日
  const endDate: Date = new Date(); // 現在の日付を取得
  const randomTimestamp: number =
    startDate.getTime() + Math.random() * (endDate.getTime() - startDate.getTime());
  const randomDate: Date = new Date(randomTimestamp);
  const year: number = randomDate.getFullYear();
  const month: number = randomDate.getMonth() + 1; // 月は0から始まるため、1を追加する
  const day: number = randomDate.getDate();

  // 日付を文字列にフォーマットして返す
  const formattedDate: string = `${year}-${month < 10 ? "0" + month : month}-${
    day < 10 ? "0" + day : day
  }`;
  return formattedDate;
};

export const getRandomTitleLang = () => {
  const random = Math.random();
  return random < 0.5 ? "ja" : "en-US";
};
