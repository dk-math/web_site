export const getScoreColorClass = (score: number) => {
  let colorClass;
  if (score >= 7.5) {
    colorClass = "green";
  } else if (score >= 5.0) {
    colorClass = "yellow";
  } else {
    colorClass = "red";
  }
  return colorClass;
};

export const getScoreBarLength = (score: number) => {
  const barLength = {
    strokeDashoffset: Math.floor(314 * 0.1 * (10 - score)),
  };

  return barLength;
};
